package StudentWithTextGrade

import kotlinx.serialization.Serializable
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.io.File

@Serializable
data class Student(
    val name:String,
    val textGrade:String,
)


val ExOne = File("clasesAbstractes/src/main/resources/ExOne.Json")
fun main(){

    val file= ExOne.readLines()
    val newProduct1 = Student("Jordi", "A")
    val newProduct2 = Student("Daniel", "C")
    val toJson1 = Json.encodeToString<Student>(newProduct1)
    val toJson2 = Json.encodeToString<Student>(newProduct2)
    ExOne.appendText("$toJson1\n")
    ExOne.appendText("$toJson2\n")

    println(newProduct1)
    println(newProduct2)
}