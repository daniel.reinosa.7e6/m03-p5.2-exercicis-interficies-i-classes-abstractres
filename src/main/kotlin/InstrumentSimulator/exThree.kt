package InstrumentSimulator

abstract class Instrument{
    abstract fun makeSounds(times:Int):String
}
class Triangle(val resonancia:Int):Instrument(){
    override fun makeSounds(times: Int):String {
        var sonido = ""
        repeat(times){
            when(resonancia){
                1 -> sonido+= "TINC "
                2 -> sonido+="TIINC "
                3 -> sonido+="TIIINC "
                4 -> sonido+="TIIIINC "
                5 -> sonido+="TIIIIINC "
            }
        }

        return sonido
    }

}
class Tambor(val to:String):Instrument(){

    override fun makeSounds(times: Int):String {
        var sonido = ""
        repeat(times){
            when(to){
                "A" -> sonido += "TAAAM "
                "O" -> sonido += "TOOOM "
                "U" -> sonido += "TUUUM "
            }
        }
        return  sonido
    }

}
fun main(){


    val instruments: List<Instrument> = listOf(
        Triangle(5),
        Tambor("A"),
        Tambor("O"),
        Triangle(1),
        Triangle(5)
    )
    play(instruments)

}
private fun play(instruments: List<Instrument>) {
    for (instrument in instruments) {
        println(instrument.makeSounds(2)) // plays 2 times the sound
    }
}