package Rainbow

import java.util.*

val scanner = Scanner(System.`in`)
fun main(){
    println("Indica un color")
    println()
    println("Input")
    val input = scanner.next().uppercase()
    val rainbowColor = listOf<String>("ROJO","NARANJA","AMARILLO","VERDE","AZUL","INDIGO","VIOLETA")
    for(color in rainbowColor){
        if (color == input){
            println("true")
            break
        }
        else if (color != input) println("false")
    }
}
