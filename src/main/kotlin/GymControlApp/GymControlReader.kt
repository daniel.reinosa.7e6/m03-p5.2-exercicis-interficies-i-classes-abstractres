package GymControlApp

import java.util.*

interface GymControlReader {
    fun nextId() : String

    class GymControlManualReader(val scanner: Scanner = Scanner(System.`in`)) : GymControlReader {
        override fun nextId() = scanner.next()
    }

}