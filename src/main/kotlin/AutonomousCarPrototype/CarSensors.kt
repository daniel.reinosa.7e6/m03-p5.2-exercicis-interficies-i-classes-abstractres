package AutonomousCarPrototype

interface CarSensors {
    enum class Direction{
        RIGHT,LEFT,FRONT
    }
    fun isThereSomethingAt(direction: Direction) : Boolean
    fun go(direction : Direction)
    fun stop()
class AutonomousCar (val CarSensors: CarSensors){
     fun doNextNSteps(n :Int){
        for (i in 1..n){
            if (isThereSomethingAt(Direction.FRONT)){
                stop()
                if (isThereSomethingAt(Direction.RIGHT)){
                    stop()
                    if (isThereSomethingAt(Direction.LEFT)){
                        stop()
                    }else {
                        go(Direction.LEFT)
                    }
                }else {
                    go(Direction.RIGHT)
                }
            }else {
                go(Direction.FRONT)
            }
        }
    }
     fun isThereSomethingAt(direction: Direction) : Boolean{
        TODO("")
    }
     fun go(direction : Direction){
        TODO("")
    }
     fun stop(){
        TODO("")
    }
}
}